# *Vice City*

This project is primarily hosted on *[GitHub](https://github.com/urbanware-org/vicecity)*.

## About this repository

While the main development and updates occur on *GitHub*, this repository is kept up-to-date regularly and you can find the latest changes here. Official releases are available on *GitHub*, only.

## Contributing

For issues as well as feature and pull requests, please visit the *GitHub* repository.
